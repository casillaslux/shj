package com.greedy.common;

public class MemberDTO {

	private int memberNum;
	private String memberId;
	private String memberPw;
	private String memberName;
	private String memberGender;
	private String memberPhoneNum;

	public MemberDTO() {}

	public MemberDTO(int memberNum, String memberId, String memberPw, String memberName, String memberGender,
			String memberPhoneNum) {
		super();
		this.memberNum = memberNum;
		this.memberId = memberId;
		this.memberPw = memberPw;
		this.memberName = memberName;
		this.memberGender = memberGender;
		this.memberPhoneNum = memberPhoneNum;
		
		
		
	}

	public int getMemberNum() {
		return memberNum;
	}

	public void setMemberNum(int memberNum) {
		this.memberNum = memberNum;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public String getMemberPw() {
		return memberPw;
	}

	public void setMemberPw(String memberPw) {
		this.memberPw = memberPw;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getMemberGender() {
		return memberGender;
	}

	public void setMemberGender(String memberGender) {
		this.memberGender = memberGender;
	}

	public String getMemberPhoneNum() {
		return memberPhoneNum;
	}

	public void setMemberPhoneNum(String memberPhoneNum) {
		this.memberPhoneNum = memberPhoneNum;
	}

	@Override
	public String toString() {
		return "MemberDTO [memberNum=" + memberNum + ", memberId=" + memberId + ", memberPw=" + memberPw
				+ ", memberName=" + memberName + ", memberGender=" + memberGender + ", memberPhoneNum=" + memberPhoneNum
				+ "]";
	}
}